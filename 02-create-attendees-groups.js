// 🚧 WIP
//node create-attendees-groups "$PARENT_GROUP_NAME" "$WORKSHOP_NAME" $MAX_ATTENDEES "$HANDSON_PROJECT_PATH" "$HANDSON_PROJECT_TARGET_NAME"

const IssueHelper = require('@tanooki/gl.cli.js/issues.helper.js').Helper
const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper
const ProjectsHelper = require('@tanooki/gl.cli.js/projects.helper').Helper

//const groupAttendeesPrefix = "grp"
const groupAttendeesPrefix = "orga"

async function exportProject({projectPath}) {
  try {
    let schedule = await ProjectsHelper.scheduleProjectExport({project: projectPath})
    console.log(schedule)
    console.log(schedule.get("message")) // should be equal to '202 Accepted'
  } catch (error) {
    console.log("😡 exportProject:", error.message)
    console.log("🖐️ parameters:", {projectPath})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function checkProjectExport({projectPath}) {
  try {
    let projectExport = await ProjectsHelper.getProjectExportStatus({project: projectPath})
    console.log(projectExport.get("id"), projectExport.get("name"), projectExport.get("export_status"))
    // projectExport.get("export_status") should be equal to 'finished'
    //console.log(projectExport.get("_links").api_url)
    //console.log(projectExport.get("_links").web_url)
    return projectExport.get("export_status")


  } catch (error) {
    console.log("😡 checkProjectExport:", error.message)
    console.log("🖐️ parameters:", {projectPath})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}

async function downloadHandsonProject({projectPath, projectName}) {

  try {
    let downloadResult = await ProjectsHelper.downloadProjectExport({project: projectPath, file:__dirname+"/"+projectName+".gz"})

    console.log(downloadResult) // == true if all is ok

  } catch (error) {
    console.log("😡 downloadHandsonProject:", error.message)
    console.log("🖐️ parameters:", {projectPath, projectName})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}

// 🚧
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}



// 🎃 🦊 importResult: {"message":{"error":"This endpoint has been requested too many times. Try again later."}}
// Create the handson project in the group of the attendee
// Rate limits on GitLab.com https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlabcom-specific-rate-limits
// https://docs.gitlab.com/ee/user/project/settings/import_export.html#rate-limits
// 6 projects per minutes
async function importHansonProject({parentGroupName, subGroupName, attendeeGroupName, projectPath, projectName}) {

  try {

    let importResult = await ProjectsHelper.importProjectFile({
      file: __dirname+"/"+projectName+".gz",
      namespace: `${parentGroupName}/${subGroupName}/${attendeeGroupName}`,
      path: projectName
      //path: projectPath
    })
    console.log("🦊 file:", __dirname+"/"+projectName+".gz")
    console.log("🦊 namespace:", `${parentGroupName}/${subGroupName}/${attendeeGroupName}`)
    console.log("🦊 path:", projectName)
    console.log("🦊 importResult:", importResult)

  } catch (error) {
    console.log("😡 importHansonProject:", error.message)
    console.log("🖐️ parameters:", {parentGroupName, subGroupName, attendeeGroupName, projectPath, projectName})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}


async function create_attendees_groups_from_registrations_issues({parentGroupName, subGroupName, maxAttendees, handsonProjectsPath, handsonProjectsList}) {
  console.log("🎃 maxAttendees -1", (maxAttendees-1))

  let projectsList = handsonProjectsList.split(",").map(project => project.trim())

  for(var member in projectsList) {
    let projectName = projectsList[member]
    // *** schedule export of the codelab project ***
    await exportProject({projectPath: handsonProjectsPath+"/"+projectName})
    // *** be sure that the export is available
    var condition = false
    /*
    setInterval(async () => {
        let exportStatus = await checkProjectExport({projectPath: handsonProjectsPath+"/"+projectName})
        console.log("👋", exportStatus)
        if(exportStatus=="finished") condition = true // clear interval
    }, 10000)
    */

    while (condition==false) {
        sleep(10000)
        let exportStatus = await checkProjectExport({projectPath: handsonProjectsPath+"/"+projectName})
        console.log("👋", exportStatus)
        if(exportStatus=="finished") condition = true
    }
    // Download the handson project
    await downloadHandsonProject({projectPath: handsonProjectsPath+"/"+projectName, projectName})

  }


  let parentGroup = await GroupsHelper.getGroupDetails({group:`${parentGroupName}/${subGroupName}`})
  let parent_group_id = parentGroup.get("id")
  console.log("👋", parent_group_id)

  let issues = await IssueHelper.getProjectIssues({project: `${parentGroupName}/${subGroupName}/registrations`})
  let firstTenIssues = issues.reverse().slice(0, (maxAttendees-1))

  /*
    GET /users?username=:username
    https://gitlab.com/api/v4/users?username=nini_queen
  */
  /* If list
  class Model {
    constructor(data) {
      this._data = data
    }

    get(field) {
      return this._data[field]
    }

    set(field, value) {
      this._data[field] = value
      return this
    }
  }

  let issues = [
    {
      id:0, iid: 0,
      author: {
        id:9176917, username:"nini_queen"
      }
    },
    {
      id:0, iid: 0,
      author: {
        id:8446436, username:"theroro"
      }
    }
  ]

  var firstTenIssues = []
  for (var member in issues) {
    firstTenIssues.push(new Model(issues[member]))
  }
  */

  // Create a group for every attendee
  for (var member in firstTenIssues) {
    let issue = firstTenIssues[member]
    console.log(issue.get("iid"), issue.get("id"), issue.get("author").username)

    // 🖐️ here, add every subscribed member to the communications project.
    try {
      let projectMember = await ProjectsHelper.addMemberToProject({
        project: `${parentGroupName}/${subGroupName}/communications`,
        userId: issue.get("author").id,
        accessLevel: 30 // dev access
      })
      console.log(projectMember)
    } catch(error) {
      console.log("😡 add member to communications project [create_attendees_groups_from_registrations_issues]:", error.message)
      console.log("🖐️ parameters:", {parentGroupName, subGroupName, maxAttendees, handsonProjectsPath, handsonProjectsList})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }


    try {
      // Create a group for the current attendee
      let attendeeGroup = await GroupsHelper.createSubGroup({
        parentGroupId: parent_group_id,
        subGroupPath: `${groupAttendeesPrefix}_${issue.get("author").username}`,
        subGroupName: `${groupAttendeesPrefix}_${issue.get("author").username}`,
        visibility: "public"
      })
      console.log("📝", attendeeGroup)

      // Add the current attendee as the owner of the group
      try {
        let groupMember = await GroupsHelper.addMemberToGroup({
          group: `${parentGroupName}/${subGroupName}/${groupAttendeesPrefix}_${issue.get("author").username}`,
          userId: issue.get("author").id,
          accessLevel: 50 // owner access
        })
        console.log("😃", groupMember)


        console.log("🎃", projectsList)
        for(var member in projectsList) {
          let projectName = projectsList[member]
          console.log("🎃", projectName)

          // Create the handson project in the group of the attendee
          await importHansonProject({
            parentGroupName,
            subGroupName,
            attendeeGroupName: `${groupAttendeesPrefix}_${issue.get("author").username}`,
            projectPath: handsonProjectsPath+"/"+projectName,
            projectName: projectName
          })

          sleep(60000/4) // max rate limit is 6 projects per minutes

        }


      } catch(error) {
        console.log("😡 add projects to the group of the attendee [create_attendees_groups_from_registrations_issues]:", error.message)
        console.log("🖐️ parameters:", {parentGroupName, subGroupName, maxAttendees, handsonProjectsPath, handsonProjectsList})
        console.log(error.response.status, error.response.statusText)
        console.log(error.response.data)
      }

    } catch(error) {
        console.log("😡 create the group of the attendee [create_attendees_groups_from_registrations_issues]:", error.message)
        console.log("🖐️ parameters:", {parentGroupName, subGroupName, maxAttendees, handsonProjectsPath, handsonProjectsList})
        console.log(error.response.status, error.response.statusText)
        console.log(error.response.data)
    }

  }
}




/*
create_attendees_groups_from_registrations_issues({
  parentGroupName: "tanooki-workshops",
  subGroupName: "WKS-DEMO-07",
  maxAttendees: 10,
  handsonProjectPath: "tanooki-workshops/workshops/hands-on-demo/amazing-project",
  handsonProjectTargetName: "amazing-project"
})
*/

/*
create_attendees_groups_from_registrations_issues({
  parentGroupName: process.argv[2],
  subGroupName: process.argv[3],
  maxAttendees: process.argv[4],
  handsonProjectPath: process.argv[5],
  handsonProjectTargetName: process.argv[6]
})
*/


create_attendees_groups_from_registrations_issues({
  parentGroupName: process.argv[2],
  subGroupName: process.argv[3],
  maxAttendees: process.argv[4],
  handsonProjectsPath: process.argv[5],
  handsonProjectsList: process.argv[6]
})



