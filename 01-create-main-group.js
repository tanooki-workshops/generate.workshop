const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper
const ProjectsHelper = require('@tanooki/gl.cli.js/projects.helper').Helper
const IssueHelper = require('@tanooki/gl.cli.js/issues.helper.js').Helper

const RepositoriesHelper = require('@tanooki/gl.cli.js/repositories.helper').Helper
const dedent = require('@tanooki/gl.cli.js').dedent

const registrationsTpl = require('./templates/registrations.readme.js')
const communicationsTpl = require('./templates/communications.readme.js')

const communicationsIssueTpl = require('./templates/communications.issue.js')


/*
  The "inscriptions" project is to provide confidential informations to the attendees
*/
async function create_registrations_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) {
  let subGroup = await GroupsHelper.getSubGroupDetails({subGroup:`${parentGroupName}/${subGroupName}`})
  let subGroupId = subGroup.get("id")
  console.log(subGroupId, subGroup)

  try {
    let project = await ProjectsHelper.createProject({
      name: "registrations",
      path: "registrations",
      nameSpaceId: subGroupId,
      initializeWithReadMe: false,
      visibility: "public"
    })
    console.log(project)

    // +++++ Create README +++++
    try {
      let markdownFile = await RepositoriesHelper.createFile({
        project: `${parentGroupName}/${subGroupName}/registrations`,
        filePath: "README.md",
        branch: "main",
        authorEmail: "pcharriere@gitlab.com",
        content: registrationsTpl.readme({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}),
        commitMessage: "📝 add the README file"
      })
      console.log(markdownFile)
    } catch(error) {
      console.log("😡 creating README [create_registrations_project]", error.message)
      console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }


  } catch(error) {
    console.log("😡 create_registrations_project:", error.message)
    console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

/*
  The "communications" project is to provide confidential informations to the attendees
*/
async function create_communications_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}) {
  let subGroup = await GroupsHelper.getSubGroupDetails({subGroup:`${parentGroupName}/${subGroupName}`})
  let subGroupId = subGroup.get("id")
  console.log(subGroupId, subGroup)

  try {
    let project = await ProjectsHelper.createProject({
      name: "communications",
      path: "communications",
      nameSpaceId: subGroupId,
      initializeWithReadMe: false,
      visibility: "private"
    })
    console.log(project)

    // +++++ Create README +++++
    try {
      let markdownFile = await RepositoriesHelper.createFile({
        project: `${parentGroupName}/${subGroupName}/communications`,
        filePath: "README.md",
        branch: "main",
        authorEmail: "pcharriere@gitlab.com",
        content: communicationsTpl.readme({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}),
        commitMessage: "📝 add the README file"
      })
      console.log(markdownFile)
    } catch(error) {
      console.log("😡 creating README [create_communications_project]", error.message)
      console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }

    // +++++ Create a confidential issue +++++
    // It allows to share confidential informations with the attendees

    // communicationsIssueTpl

    let confidentialIssue = await IssueHelper.createIssue({
      project: `${parentGroupName}/${subGroupName}/communications`,
      title: communicationsIssueTpl.title({workShopDescription, workshopDate}),
      description: communicationsIssueTpl.body({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees}),
      confidential: true
    })

  } catch(error) {
    console.log("😡 create_communications_project:", error.message)
    console.log("🖐️ parameters:", {parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function exportProject({projectPath}) {
  try {
    let schedule = await ProjectsHelper.scheduleProjectExport({project: projectPath})
    console.log(schedule)
    console.log(schedule.get("message")) // should be equal to '202 Accepted'
  } catch (error) {
    console.log("😡 exportProject:", error.message)
    console.log("🖐️ parameters:", {projectPath})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function checkProjectExport({projectPath}) {
  try {
    let projectExport = await ProjectsHelper.getProjectExportStatus({project: projectPath})
    console.log(projectExport.get("id"), projectExport.get("name"), projectExport.get("export_status"))
    // projectExport.get("export_status") should be equal to 'finished'
    //console.log(projectExport.get("_links").api_url)
    //console.log(projectExport.get("_links").web_url)

    return projectExport.get("export_status")

  } catch (error) {
    console.log("😡 checkProjectExport:", error.message)
    console.log("🖐️ parameters:", {projectPath})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}


async function downloadThenImportProject({projectPath, targetName, parentGroupName, subGroupName}) {

  try {
    let downloadResult = await ProjectsHelper.downloadProjectExport({project: projectPath, file:__dirname+"/"+targetName+".gz"})

    console.log(downloadResult) // == true if all is ok

    let importResult = await ProjectsHelper.importProjectFile({
      file: __dirname+"/"+targetName+".gz",
      namespace: `${parentGroupName}/${subGroupName}`,
      path: targetName
    })
    console.log(importResult)

  } catch (error) {
    console.log("😡 downloadThenImportProject:", error.message)
    console.log("🖐️ parameters:", {projectPath, targetName, parentGroupName, subGroupName})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function triggerPagesPipeline({parentGroupName, subGroupName, targetName}) {
  // +++++ Create a file to trigger the pipeline +++++
  try {
    let noteFile = await RepositoriesHelper.createFile({
      project: `${parentGroupName}/${subGroupName}/${targetName}`,
      filePath: "note.txt",
      branch: "main",
      authorEmail: "pcharriere@gitlab.com",
      content: "this file was created to trigger the pipeline",
      commitMessage: "📝 add the note.txt file"
    })
    console.log(noteFile)
  } catch(error) {
    console.log("😡 triggerPagesPipeline:", error.message)
    console.log("🖐️ parameters:", {parentGroupName, subGroupName, targetName})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}


async function create_main_sub_group({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees, codelabPath, landingPagePath}) {

  let parentGroup = await GroupsHelper.getGroupDetails({group:parentGroupName})
  let parent_group_id = parentGroup.get("id")
  console.log(parent_group_id)

  try {
    let workShopGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: subGroupName,
      subGroupName: subGroupName,
      visibility: "public"
    })
    console.log("📝", workShopGroup)

    // *** create inscriptions project ***
    await create_registrations_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})

    // *** create communications project ***
    await create_communications_project({parentGroupName, subGroupName, workShopDescription, workshopDate, maxAttendees})

    /* Right now I don't use the codelab tool
    // *** schedule export of the codelab project ***
    await exportProject({projectPath: codelabPath})

    // *** be sure that the export is available
    var condition = false
    while (condition==false) {
      let exportStatus = await checkProjectExport({projectPath: codelabPath})
      console.log("👋", exportStatus)
      if(exportStatus=="finished") condition = true
    }
    // *** then import the codelab project in the workshop group
    await downloadThenImportProject({projectPath: codelabPath, targetName: "codelab", parentGroupName, subGroupName})
    console.log("🖐️ Codelab is exported, don't forget to run its pipeline to publish the page")
    // Trigger the pipeline
    setTimeout(async () => {
      await triggerPagesPipeline({parentGroupName, subGroupName, targetName: "codelab"})
    }, 5000);
    */


    // *** schedule export of the landing-page project ***
    await exportProject({projectPath: landingPagePath})

    // *** be sure that the export is available
    var condition = false
    while (condition==false) {
      let exportStatus = await checkProjectExport({projectPath: landingPagePath})
      console.log("👋", exportStatus)
      if(exportStatus=="finished") condition = true
    }
    // *** then import the codelab project in the workshop group
    await downloadThenImportProject({projectPath: landingPagePath, targetName: "landing-page", parentGroupName, subGroupName})
    console.log("🖐️ Landing-Page is exported, don't forget to run its pipeline to publish the page")
    // Trigger the pipeline
    setTimeout(async () => {
      await triggerPagesPipeline({parentGroupName, subGroupName, targetName: "landing-page"})
    }, 5000);


  } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }

}

/*
create_main_sub_group({
  parentGroupName: "tanooki-workshops",
  subGroupName: "WKS-DEMO-07",
  workShopDescription: "Amazing workshop",
  maxAttendees: 10,
  codelabPath: "tanooki-workshops/workshops/hands-on-demo/codelab", // Location of the codelab project
  landingPagePath: "tanooki-workshops/workshops/hands-on-demo/landing-page" // Location of the landing-page project
})
*/

create_main_sub_group({
  parentGroupName: process.argv[2],
  subGroupName: process.argv[3],
  workShopDescription: process.argv[4],
  workshopDate: process.argv[5],
  maxAttendees: process.argv[6],
  codelabPath: process.argv[7], // Location of the codelab project
  landingPagePath: process.argv[8] // Location of the landing-page project
})

